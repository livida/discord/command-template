# Command Template
Would you like to get your own custom command or event on the Livida Bot? Follow these instructions below to find out more!
### Format
For more details of the format of our commands, check the [command.js](command.js) file and for events checkout [event.js](/event.js).
### Submission
To submit a command, fork this project, add your new command into the [commands](/commands) directory, then make a pull request back to this original branch. You can also do the same for events but please place it in the [events](/events) directory.
